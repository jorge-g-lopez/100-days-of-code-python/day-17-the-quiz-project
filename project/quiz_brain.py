"""Quiz Brain"""


class QuizBrain:
    """QuizBrain class"""

    def __init__(self, question_list):
        self.question_number = 0
        self.score = 0
        self.question_list = question_list

    def next_question(self):
        """Ask the current question and check the answer"""
        questiontext = "Q." + str(self.question_number + 1) + ": "
        questiontext += self.question_list[self.question_number].text
        questiontext += " (True/False)?: "
        answer = input(questiontext)
        self.check_answer(answer, self.question_list[self.question_number].answer)
        self.question_number += 1

    def check_answer(self, user_answer, correct_answer):
        """Check the answer and provide feedback"""
        if user_answer.lower() == correct_answer.lower():
            print("You got it right!")
            self.score += 1
        else:
            print("That's wrong.")
        current_score = str(self.score) + "/" + str(self.question_number + 1)
        print(f"The correct answer was: {correct_answer}.")
        print(f"Your current score is: {current_score}.")
        print("\n")

    def still_has_questions(self):
        """Return True if there are still questions left"""
        return len(self.question_list) > self.question_number
