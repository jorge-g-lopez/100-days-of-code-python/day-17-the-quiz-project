"""The Quiz Project"""

from data import QUESTION_DATA
from question_model import Question
from quiz_brain import QuizBrain

question_bank = []

for question in QUESTION_DATA:
    question_bank.append(Question(question["text"], question["answer"]))

my_quizbrain = QuizBrain(question_bank)

while my_quizbrain.still_has_questions():
    my_quizbrain.next_question()

final_score = str(my_quizbrain.score) + "/" + str(my_quizbrain.question_number)
print("You have completed the quiz.")
print(f"Your final score was: {final_score}.")
